import { Given, When, Then } from 'cucumber';
import { LoadUsersPage } from '../pages/LoadUsersPage.po';
import { UserManagementPage } from '../pages/UserManagementPage.po';
import { newUser, editUser } from '../data/constants';
import { routes } from '../data/routes';
import { expect } from '../utils/chai';
import { BrowserActions } from '../utils/browser';

const { firstName, lastName, userName, primaryEmail } = newUser;
const { newUserName } = editUser;
const { baseURL } = routes;

const loadUsersPage: LoadUsersPage = new LoadUsersPage();
const userManagementPage: UserManagementPage = new UserManagementPage();

Given(/^The User is on the "Load Users" page$/, async () => {
    await BrowserActions.navigateTo(baseURL);
});

Given(/^clicks on the "Add User" button$/, async () => {
    await loadUsersPage.openAddUserForm();
});

When(/^he fills in all required fields and submit the form$/, async () => {
    await userManagementPage.addNewUser(firstName, lastName, userName, primaryEmail);
});

Then(/^the page contains a message which says "([^"]*)"$/, async (header: string) => {
    expect(await userManagementPage.getPopupTextHeader()).equal(header);
    await userManagementPage.confirmPopup();
});

Then(/^a new record with provided information is added to the "Users" table$/, async () => {
    await loadUsersPage.searchBy(userName);
    expect(await userManagementPage.getSearchResultsCount()).equal(1);
    expect(await userManagementPage.getSearchResultsValues())
        .to.be.an('array')
        .that.to.include.members([userName]);
});

Given(/^finds the User for editing then clicks on the "Edit" button$/, async () => {
    await loadUsersPage.searchBy(userName);
    await loadUsersPage.openEditUserForm();
});

When(/^he fills in the "Username" field and submit the form$/, async () => {
    await userManagementPage.editUser(newUserName);
});

Then(/^a record is updated with provided information in the "Users" table$/, async () => {
    await loadUsersPage.searchBy(newUserName);
    expect(await userManagementPage.getSearchResultsCount()).equal(1);
    expect(await userManagementPage.getSearchResultsValues())
        .to.be.an('array')
        .that.to.include.members([newUserName]);
});

Given(/^finds the User for deleting then clicks on the "Edit" button$/, async () => {
    await loadUsersPage.searchBy(newUserName);
    await loadUsersPage.openEditUserForm();
});

When(/^he clicks the "Delete" button and confirms a popup$/, async () => {
    await userManagementPage.deleteUser();
});

Then(/^a record with given user's details is deleted from the "Users" table$/, async () => {
    await loadUsersPage.searchBy(newUserName);
    expect(await userManagementPage.getSearchResultsCount()).equal(0);
});
