import { Given, When, Then } from 'cucumber';
import { LoadUsersPage } from '../pages/LoadUsersPage.po';
import { AlarmGroupManagementPage } from '../pages/AlarmGroupsManagementPage.po';
import { newAlarmGroup } from '../data/constants';
import { expect } from '../utils/chai';

const { newAlarmGroupName } = newAlarmGroup;

const loadUsersPage: LoadUsersPage = new LoadUsersPage();
const alarmGroupsManagementPage: AlarmGroupManagementPage = new AlarmGroupManagementPage();

Given(/^clicks on the "Add Alarm Group" button$/, async () => {
    await loadUsersPage.openAddAlarmGroupForm();
});

When(/^he fills in "Group Name" field and submit the form$/, async () => {
    await alarmGroupsManagementPage.addNewAlarmGroup(newAlarmGroupName);
});

When(/^clicks on the "Edit Alarm Group" button$/, async () => {
    await loadUsersPage.openEditAlarmGroupForm();
});

Then(/^the "Alarm Groups" dropdown contains a newly created alarm group$/, async () => {
    expect(await alarmGroupsManagementPage.getAlarmGroupsList())
        .to.be.an('array')
        .that.to.include.members([newAlarmGroupName]);
});

When(
    /^he chooses the User from the "User Groups" user's list and a particular alarm group from dropdown$/,
    async () => {
        await alarmGroupsManagementPage.selectAlarmGroup(newAlarmGroupName);
        await alarmGroupsManagementPage.selectFirstUserFromUsersTable();
        expect(await alarmGroupsManagementPage.getUsersCount()).equal(0);
    },
);

When(/^clicks on the "Move" button then submit the form$/, async () => {
    await alarmGroupsManagementPage.moveUser();
});

Then(/^the User is displayed among users of this group$/, async () => {
    await loadUsersPage.openEditAlarmGroupForm();
    await alarmGroupsManagementPage.selectAlarmGroup(newAlarmGroupName);
    expect(await alarmGroupsManagementPage.getUsersCount()).equal(1);
});

When(
    /^he chooses the User from the "Alarm Groups" user's list and a particular alarm group from dropdown$/,
    async () => {
        await alarmGroupsManagementPage.selectAlarmGroup(newAlarmGroupName);
        await alarmGroupsManagementPage.selectFirstUserFromAlarmGroupTable();
        expect(await alarmGroupsManagementPage.getUsersCount()).equal(1);
    },
);

When(/^clicks on the "Remove Selected Users" button then submit the form$/, async () => {
    await alarmGroupsManagementPage.removeUser();
});

Then(/^the User is removed from users of this group$/, async () => {
    await loadUsersPage.openEditAlarmGroupForm();
    await alarmGroupsManagementPage.selectAlarmGroup(newAlarmGroupName);
    expect(await alarmGroupsManagementPage.getUsersCount()).equal(0);
});
