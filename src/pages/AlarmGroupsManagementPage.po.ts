import { ElementFinder, element, by, ElementArrayFinder } from 'protractor';
import { Actions } from '../utils/actions';
import { CustomWaits } from '../utils/waits';

export class AlarmGroupManagementPage {
    public groupName: ElementFinder;
    public saveBtn: ElementFinder;
    public progressbar: ElementFinder;
    public mask: ElementFinder;
    public alarmGroupList: ElementFinder;
    public alarmGroupListValues: ElementArrayFinder;
    public alarmGroupDropdown: ElementFinder;
    public moveBtn: ElementFinder;
    public removeSelectedUsersBtn: ElementFinder;
    public firstRowInUsersTable: ElementFinder;
    public firstRowInAlarmGroupTable: ElementFinder;
    public users: ElementArrayFinder;

    constructor() {
        this.groupName = element(by.name('groupName'));
        this.saveBtn = element(by.xpath("//span[contains(text(),'Save')]/ancestor-or-self::a"));
        this.progressbar = element(by.xpath("//*[@role='progressbar']"));
        this.mask = element
            .all(by.xpath("//*[contains(@class, 'x-mask x-border-box') and contains(@role, 'progressbar')]"))
            .last();
        this.alarmGroupList = element(by.xpath("//*[@role = 'listbox']"));
        this.alarmGroupListValues = element.all(by.xpath("//*[@role = 'listbox']/child::li"));
        this.alarmGroupDropdown = element(by.xpath("//input[@value = 'Alarm Group 1']"));
        this.moveBtn = element(by.xpath("//span[contains(text(),'Move')]/ancestor-or-self::a"));
        this.removeSelectedUsersBtn = element(
            by.xpath("//span[contains(text(),'Remove Selected Users')]/ancestor-or-self::a"),
        );
        this.firstRowInAlarmGroupTable = element
            .all(
                by.xpath(
                    "//*[contains(., 'Users') and contains(@class, 'x-title-text')]/ancestor::fieldset[@aria-label='Alarm Groups field set']//tr",
                ),
            )
            .first();
        this.firstRowInUsersTable = element
            .all(
                by.xpath(
                    "//*[contains(., 'Users') and contains(@class, 'x-title-text')]/ancestor::fieldset[@aria-label='User Groups field set']//tr",
                ),
            )
            .first();
        this.users = element.all(
            by.xpath(
                "//*[contains(., 'Users') and contains(@class, 'x-title-text')]/ancestor::fieldset[@aria-label='Alarm Groups field set']//tr",
            ),
        );
    }

    async fillGroupNameField(groupName: string): Promise<void> {
        await Actions.clearFieldInput(this.groupName);
        await Actions.fillInTheField(this.groupName, groupName);
    }

    async submitAddAlarmGroupForm(): Promise<void> {
        await Actions.click(this.saveBtn);
    }

    async addNewAlarmGroup(groupName: string): Promise<void> {
        await this.fillGroupNameField(groupName);
        await this.submitAddAlarmGroupForm();
        await CustomWaits.isNotVisible(this.mask, 10000);
    }

    async getAlarmGroupsList(): Promise<string[]> {
        await Actions.click(this.alarmGroupDropdown);
        await CustomWaits.isVisible(this.alarmGroupList, 10000);
        return await Actions.getAllText(this.alarmGroupListValues);
    }

    async selectAlarmGroup(newAlarmGroupName: string): Promise<void> {
        await Actions.click(this.alarmGroupDropdown);
        await CustomWaits.isVisible(this.alarmGroupList, 10000);
        const select = element(by.xpath(`//li[contains(., "${newAlarmGroupName}")]`));
        await CustomWaits.inDOM(select, 10000);
        await Actions.scrollTo(select);
        await Actions.click(select);
        await CustomWaits.isNotVisible(this.mask, 15000);
    }

    async selectFirstUserFromUsersTable(): Promise<void> {
        await Actions.click(this.firstRowInUsersTable);
    }

    async selectFirstUserFromAlarmGroupTable(): Promise<void> {
        await Actions.click(this.firstRowInAlarmGroupTable);
    }

    async submitEditUserForm(): Promise<void> {
        await Actions.click(this.saveBtn);
    }

    async moveUser(): Promise<void> {
        await Actions.click(this.moveBtn);
        await this.submitEditUserForm();
        await CustomWaits.waitForLoad(4000);
    }

    async removeUser(): Promise<void> {
        await Actions.click(this.removeSelectedUsersBtn);
        await this.submitEditUserForm();
        await CustomWaits.waitForLoad(4000);
    }

    async getUsersCount(): Promise<number> {
        return await this.users.count();
    }
}
