import { ElementFinder, element, by, ElementArrayFinder } from 'protractor';
import { Actions } from '../utils/actions';
import { CustomWaits } from '../utils/waits';

export class UserManagementPage {
    public firstName: ElementFinder;
    public lastName: ElementFinder;
    public userName: ElementFinder;
    public primaryEmail: ElementFinder;
    public saveBtn: ElementFinder;
    public popUpHeader: ElementFinder;
    public popUpConfirmBtn: ElementFinder;
    public progressbar: ElementFinder;
    public mask: ElementFinder;
    public searchResults: ElementArrayFinder;
    public searchResultsValues: ElementArrayFinder;
    public deleteBtn: ElementFinder;
    public confirmDeletingBtn: ElementFinder;

    constructor() {
        this.firstName = element(by.name('firstName'));
        this.lastName = element(by.name('lastName'));
        this.userName = element(by.name('userName'));
        this.primaryEmail = element(by.name('primaryEmail'));
        this.saveBtn = element(by.xpath("//span[contains(text(),'Save')]/ancestor-or-self::a"));
        this.popUpHeader = element(
            by.xpath("//*[contains(@id, 'messagebox') and contains(@id, 'header-title-textEl')]"),
        );
        this.popUpConfirmBtn = element(by.xpath("//*[contains(text(), 'OK')]"));
        this.progressbar = element(by.xpath("//*[@role='progressbar']"));
        this.mask = element
            .all(by.xpath("//*[contains(@class, 'x-mask x-border-box') and contains(@role, 'presentation')]"))
            .last();
        this.searchResults = element.all(by.tagName('tr'));
        this.searchResultsValues = element
            .all(by.tagName('tr'))
            .all(by.xpath("//td[contains(@class, 'x-grid-td')]/child::div[@class='x-grid-cell-inner ']"));
        this.deleteBtn = element(by.xpath("//span[contains(text(),'Delete')]/ancestor-or-self::a"));
        this.confirmDeletingBtn = element(by.xpath("//span[contains(text(),'Yes')]/ancestor-or-self::a"));
    }

    async fillFirstNameField(firstName: string): Promise<void> {
        await Actions.clearFieldInput(this.firstName);
        await Actions.fillInTheField(this.firstName, firstName);
    }

    async fillLastNameField(lastName: string): Promise<void> {
        await Actions.clearFieldInput(this.lastName);
        await Actions.fillInTheField(this.lastName, lastName);
    }

    async fillUserNameField(userName: string): Promise<void> {
        await Actions.clearFieldInput(this.userName);
        await Actions.fillInTheField(this.userName, userName);
    }

    async fillPrimaryEmailField(primaryEmail: string): Promise<void> {
        await Actions.clearFieldInput(this.primaryEmail);
        await Actions.fillInTheField(this.primaryEmail, primaryEmail);
    }

    async submitAddUserForm(): Promise<void> {
        await Actions.click(this.saveBtn);
    }

    async submitEditUserForm(): Promise<void> {
        await Actions.click(this.saveBtn);
    }

    async addNewUser(firstName: string, lastName: string, userName: string, primaryEmail: string): Promise<void> {
        await this.fillFirstNameField(firstName);
        await this.fillLastNameField(lastName);
        await this.fillUserNameField(userName);
        await this.fillPrimaryEmailField(primaryEmail);
        await this.submitAddUserForm();
    }

    async getPopupTextHeader(): Promise<string> {
        return await Actions.getTextFromElement(this.popUpHeader);
    }

    async confirmPopup(): Promise<void> {
        await Actions.click(this.popUpConfirmBtn);
        await CustomWaits.isNotVisible(this.popUpHeader, 3000);
        await CustomWaits.isNotVisible(this.mask, 25000);
        await CustomWaits.isNotVisible(this.progressbar, 15000);
    }

    async confirmDeletingPopup(): Promise<void> {
        await Actions.click(this.confirmDeletingBtn);
        await CustomWaits.isNotVisible(this.popUpHeader, 3000);
        await CustomWaits.isNotVisible(this.mask, 25000);
        await CustomWaits.isNotVisible(this.progressbar, 15000);
    }

    async getSearchResultsCount(): Promise<number> {
        return await this.searchResults.count();
    }

    async getSearchResultsValues(): Promise<string[]> {
        return await Actions.getAllText(this.searchResultsValues);
    }

    async editUser(userName: string): Promise<void> {
        await this.fillUserNameField(userName);
        await this.submitEditUserForm();
        await CustomWaits.isNotVisible(this.mask, 25000);
        await CustomWaits.isNotVisible(this.progressbar, 15000);
    }

    async deleteUser(): Promise<void> {
        await Actions.click(this.deleteBtn);
        await this.confirmDeletingPopup();
        await CustomWaits.isNotVisible(this.mask, 25000);
        await CustomWaits.isNotVisible(this.progressbar, 15000);
    }
}
