import { element, by, ElementFinder } from 'protractor';
import { CustomWaits } from '../utils/waits';
import { Actions } from '../utils/actions';

export class LoadUsersPage {
    public preloader: ElementFinder;
    public preloadingMask: ElementFinder;
    public progressbar: ElementFinder;
    public addUserBtn: ElementFinder;
    public addUserForm: ElementFinder;
    public searchByUsername: ElementFinder;
    public editUserBtn: ElementFinder;
    public editUserForm: ElementFinder;
    public addAlarmGroupBtn: ElementFinder;
    public addAlarmGroupForm: ElementFinder;
    public editAlarmGroupBtn: ElementFinder;
    public editAlarmGroupForm: ElementFinder;

    constructor() {
        this.preloader = element(by.id('loading-mask2'));
        this.preloadingMask = element(by.id('loading-mask'));
        this.progressbar = element(by.xpath("//*[@role='progressbar']"));
        this.addUserBtn = element(by.xpath("//span[contains(text(), 'Add User')]/ancestor-or-self::a"));
        this.addUserForm = element(by.xpath("//input[contains(@name,'firstName')]"));
        this.searchByUsername = element(
            by.xpath(
                "//span[starts-with(@id, 'gridcolumn') and contains(., 'Username')]/ancestor::div[contains(@class, 'x-column-header')]//input",
            ),
        );
        this.editUserBtn = element(
            by.xpath("//*[contains(., 'Edit')]/ancestor::a[contains(@class, 'x-btn-default-small')]"),
        );
        this.editUserForm = element(by.xpath("//input[contains(@name,'userName')]"));
        this.addAlarmGroupBtn = element(by.xpath("//span[contains(text(), 'Add Alarm Group')]/ancestor-or-self::a"));
        this.addAlarmGroupForm = element(by.xpath("//input[contains(@name,'groupName')]"));
        this.editAlarmGroupBtn = element(by.xpath("//span[contains(text(), 'Edit Alarm Group')]/ancestor-or-self::a"));
        this.editAlarmGroupForm = element(by.xpath("//*[@aria-label='Alarm Groups field set']"));
    }

    async openAddUserForm(): Promise<void> {
        await CustomWaits.isNotVisible(this.preloader, 15000);
        await CustomWaits.isNotVisible(this.preloadingMask, 15000);
        await CustomWaits.isNotVisible(this.progressbar, 30000);
        await Actions.click(this.addUserBtn);
        await CustomWaits.isVisible(this.addUserForm, 5000);
    }

    async searchBy(userName: string): Promise<void> {
        await CustomWaits.isNotVisible(this.preloader, 15000);
        await CustomWaits.isNotVisible(this.preloadingMask, 15000);
        await CustomWaits.isNotVisible(this.progressbar, 30000);
        await Actions.clearFieldInput(this.searchByUsername);
        await Actions.fillInTheField(this.searchByUsername, userName);
        await CustomWaits.waitForLoad(1500);
    }

    async openEditUserForm(): Promise<void> {
        await CustomWaits.isNotVisible(this.preloader, 15000);
        await CustomWaits.isNotVisible(this.preloadingMask, 15000);
        await CustomWaits.isNotVisible(this.progressbar, 30000);
        await Actions.click(this.editUserBtn);
        await CustomWaits.isVisible(this.editUserForm, 5000);
    }

    async openAddAlarmGroupForm(): Promise<void> {
        await CustomWaits.isNotVisible(this.preloader, 15000);
        await CustomWaits.isNotVisible(this.preloadingMask, 15000);
        await CustomWaits.isNotVisible(this.progressbar, 30000);
        await Actions.click(this.addAlarmGroupBtn);
        await CustomWaits.isVisible(this.addAlarmGroupForm, 5000);
    }

    async openEditAlarmGroupForm(): Promise<void> {
        await CustomWaits.isNotVisible(this.preloader, 15000);
        await CustomWaits.isNotVisible(this.preloadingMask, 15000);
        await CustomWaits.isNotVisible(this.progressbar, 30000);
        await Actions.click(this.editAlarmGroupBtn);
        await CustomWaits.isVisible(this.editAlarmGroupForm, 10000);
    }
}
