import * as faker from 'faker';

export const newUser = {
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    userName: faker.internet.userName(),
    primaryEmail: '',
};

export const editUser = {
    newUserName: faker.internet.userName(),
};

export const newAlarmGroup = {
    newAlarmGroupName: faker.random.uuid(),
};
