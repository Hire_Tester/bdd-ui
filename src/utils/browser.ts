import { browser, ElementFinder } from 'protractor';
import { logThisMethod } from './logging-decorator';

class BrowserActions {
    static MEDIUM_TIMEOUT = 15000;
    static SLOW_DOWN = 500;

    @logThisMethod
    public static async navigateTo(url: string): Promise<void> {
        await browser.get(url);
    }

    @logThisMethod
    public static async switchTo(element: ElementFinder): Promise<void> {
        await browser.switchTo().frame(element.getWebElement());
    }

    @logThisMethod
    public static async switchToDefault(): Promise<void> {
        await browser.switchTo().defaultContent();
    }

    @logThisMethod
    public static async switchToTab(tabIdx: number): Promise<void> {
        const handles = await browser.getAllWindowHandles();
        await browser.switchTo().window(handles[tabIdx]);
    }

    @logThisMethod
    public static async scrollBottom(element: ElementFinder): Promise<void> {
        await browser.executeScript('var p = arguments[0]; p.scrollTop=p.scrollHeights;', element);
    }

    @logThisMethod
    public static async closeTab(): Promise<void> {
        await browser.manage().deleteAllCookies();
        await browser.close();
    }

    @logThisMethod
    public static async clearBrowserData(): Promise<void> {
        await browser.manage().deleteAllCookies();
        await browser.executeScript('window.sessionStorage.clear();');
        await browser.executeScript('window.localStorage.clear();');
    }
}

export { BrowserActions };
