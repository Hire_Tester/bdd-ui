import * as winston from 'winston';
import { createLogger, format } from 'winston';

const level = process.env.LOG_LEVEL || 'debug';

const logger = createLogger({
    level: level,
    format: format.combine(format.simple()),
    transports: [
        new winston.transports.File({ filename: '.tmp/test.log', level: 'debug' }),
        new winston.transports.Console(),
    ],
});

export { logger };
