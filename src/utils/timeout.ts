/* eslint-disable @typescript-eslint/no-var-requires */
const { setDefaultTimeout } = require('cucumber');

setDefaultTimeout(60 * 1000);
