import { convert } from 'cucumber-junit-convert';

const options = {
    inputJsonFile: '.tmp/results.json',
    outputXmlFile: '.tmp/results.xml',
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function generateXmlReport(): any {
    convert(options);
}
