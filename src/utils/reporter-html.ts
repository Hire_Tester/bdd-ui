/* eslint-disable @typescript-eslint/explicit-function-return-type */
import * as reporter from 'cucumber-html-reporter';
import * as path from 'path';
const jsonReports = path.join(process.cwd(), '.tmp');
const targetJson = jsonReports + '/results.json';

const cucumberReporterOptions = {
    jsonFile: targetJson,
    output: jsonReports + '/results.html',
    reportSuiteAsScenarios: true,
    theme: 'bootstrap',
    launchReport: true,
};

export class Reporter {
    public static createHTMLReport() {
        try {
            reporter.generate(cucumberReporterOptions); // invoke cucumber-html-reporter
        } catch (err) {
            if (err) {
                throw new Error('Failed to save cucumber test results to json file.');
            }
        }
    }
}
