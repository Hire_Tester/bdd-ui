import { browser, ExpectedConditions as EC, ElementFinder, ElementArrayFinder } from 'protractor';
import { logger } from './logger';
import { logThisMethod } from './logging-decorator';
import { World } from 'cucumber';
import { CustomWaits } from './waits';

class Actions {
    static MEDIUM_TIMEOUT = 15000;
    static SLOW_DOWN = 500;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public static log(...message: any[]): void {
        logger.debug(message);
    }

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    public static async attachScreenshot(world: World) {
        const screenShot = await browser.takeScreenshot();
        world.attach(screenShot, 'image/png');
    }

    @logThisMethod
    public static async isClickable(element: ElementFinder, ms: number): Promise<boolean> {
        return await browser.wait(EC.elementToBeClickable(element), ms);
    }

    public static async highlightElement(element: ElementFinder): Promise<void> {
        return await browser.executeScript("arguments[0].style.border='3px solid #FFA500'", element);
    }

    @logThisMethod
    public static async fillInTheField(element: ElementFinder, text: string): Promise<void> {
        await CustomWaits.isVisible(element, this.MEDIUM_TIMEOUT);
        return await element.sendKeys(text);
    }

    @logThisMethod
    public static async click(element: ElementFinder): Promise<void> {
        await CustomWaits.isVisible(element, this.MEDIUM_TIMEOUT);
        await this.highlightElement(element);
        return await element.click();
    }

    @logThisMethod
    public static async clearFieldInput(element: ElementFinder): Promise<void> {
        await this.click(element);
        return await element.clear();
    }

    @logThisMethod
    public static async getElementByText(elements: ElementArrayFinder, name: string): Promise<void> {
        return await elements
            .filter(async function(element) {
                return (await element.getText()) === name;
            })
            .get(0);
    }

    @logThisMethod
    public static async getTextFromElement(element: ElementFinder): Promise<string> {
        return await element.getText();
    }

    @logThisMethod
    public static async scrollTo(elementToScroll: ElementFinder): Promise<void> {
        return await browser.executeScript('arguments[0].scrollIntoView();', elementToScroll);
    }

    @logThisMethod
    public static async getAllText(elements: ElementArrayFinder): Promise<string[]> {
        return await elements.map(async (element: ElementFinder) => {
            return await element.getText();
        });
    }
}

export { Actions };
