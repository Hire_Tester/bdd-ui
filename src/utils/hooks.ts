import { Status, HookScenarioResult, After, AfterAll } from 'cucumber';

import { Actions } from './actions';
import { BrowserActions } from './browser';

After(async function(scenario: HookScenarioResult): Promise<void> {
    if (scenario.result.status === Status.FAILED) {
        await Actions.attachScreenshot(this);
    }
});

AfterAll(async function() {
    await BrowserActions.clearBrowserData();
});
