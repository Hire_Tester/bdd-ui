import { browser, ExpectedConditions as EC, ElementFinder } from 'protractor';

class CustomWaits {
    public static async hasText(element: ElementFinder, text: string, ms: number): Promise<boolean> {
        return await browser.wait(EC.textToBePresentInElement(element, text), ms);
    }

    public static async isVisible(element: ElementFinder, ms: number): Promise<boolean> {
        return await browser.wait(
            EC.visibilityOf(element),
            ms,
            `Failed while waiting ${ms}ms for element ${element.locator()}`,
        );
    }

    public static async isNotVisible(element: ElementFinder, ms: number): Promise<boolean> {
        return await browser.wait(
            EC.invisibilityOf(element),
            ms,
            `Failed while waiting ${ms}ms for element ${element.locator()}`,
        );
    }

    public static async inDOM(element: ElementFinder, ms: number): Promise<boolean> {
        return await browser.wait(
            EC.presenceOf(element),
            ms,
            `Failed while waiting ${ms}ms for element ${element.locator()}`,
        );
    }

    public static async notInDOM(element: ElementFinder, ms: number): Promise<boolean> {
        return await browser.wait(
            EC.stalenessOf(element),
            ms,
            `Failed while waiting ${ms}ms for element ${element.locator()}`,
        );
    }

    public static async hasAttributeValue(item: ElementFinder, attribute: string, ms: number): Promise<string> {
        return await browser.wait(async function() {
            return await item.getAttribute(attribute);
        }, ms);
    }

    public static async and(condition: Function, condition2: Function, ms: number): Promise<boolean> {
        return await browser.wait(EC.and(condition, condition2), ms);
    }

    public static async isClickable(element: ElementFinder, ms: number): Promise<boolean> {
        return await browser.wait(EC.elementToBeClickable(element), ms);
    }

    public static async waitForLoad(ms: number): Promise<void> {
        await browser.sleep(ms);
    }
}

export { CustomWaits };
