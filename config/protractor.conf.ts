import { browser, Config } from 'protractor';
import { generateXmlReport } from '../src/utils/reporter-xml';
import { Reporter } from '../src/utils/reporter-html';
const argv = require('yargs').argv;

export const config: Config = {
    directConnect: true,
    SELENIUM_PROMISE_MANAGER: false,

    capabilities: {
        shardTestFiles: false,
        maxInstances: 1,
        browserName: 'chrome',
        restartBrowserBetweenTests: false,
        chromeOptions: {
            args: ['--no-sandbox', '--test-type=browser'],
        },
    },

    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    specs: ['../features/*.feature'],

    onPrepare: () => {
        browser.ignoreSynchronization = true;
        browser
            .manage()
            .window()
            .maximize();
        browser
            .manage()
            .timeouts()
            .implicitlyWait(5000);
        browser
            .manage()
            .timeouts()
            .setScriptTimeout(30000);
    },

    cucumberOpts: {
        compiler: 'ts:ts-node/register',
        format: ['json:.tmp/results.json', 'node_modules/cucumber-pretty'],
        require: ['../src/steps/*.ts', '../src/utils/*.ts'],
        strict: true,
        tags: argv.tags || '',
    },

    onCleanUp: () => {
        generateXmlReport();
        Reporter.createHTMLReport();
    },
};
