# Protractor-Cucumber-TypeScript Setup Guide

## Project Description:
* Project setup with Protractor version 5.4.2. A config file is in `./config` directory.
* All scripts written with Typescript & Cucumber.
* Page Object classes are in the `./src/pages` directory.
* Mock data (eg firstName, lastName) are in `./src/data/constants`.
* Utility functions are in `./src/utils` directory.
* HTML and XML reports are in `./tmp` directory.
* No need to compile TypeScript testfiles before running.
* Screenshots on failure feature scenarios.

It supports and provides:

* [protractor](https://github.com/angular/protractor)
* [cucumberjs](https://github.com/cucumber/cucumber-js/)
* [typescript](https://github.com/Microsoft/TypeScript)
* [cucumber-html-reporter](https://github.com/gkushang/cucumber-html-reporter)

### To Get Started

#### Pre-requisites
1. NodeJS installed globally in the system.
https://nodejs.org/en/download/

2. `npm install protractor -g` to install protractor globally

3. Chrome or Firefox browsers installed.

#### Setup Scripts
* Clone the repository into a folder
* Go inside the folder and run following command from terminal/command prompt
```
npm install
```
* All the dependencies from package.json and ambient typings would be installed in node_modules folder.

#### Run Scripts

* Now just run the test command which launches a browser and runs the scripts. Defaults to using Chrome browser.
```
npm test
```

#### Writing Features
```
Feature: Alarm groups Management

    @smoke
    Scenario: Verify that a new alarm group is created after submitting the "New Alarm Group" form
        Given The User is on the "Load Users" page
        And clicks on the "Add Alarm Group" button
        When he fills in "Group Name" field and submit the form
        And clicks on the "Edit Alarm Group" button
        Then the "Alarm Groups" dropdown contains a newly created alarm group
```
#### Writing Step Definitions

```
import { Given, When, Then } from 'cucumber';
import { LoadUsersPage } from '../pages/LoadUsersPage.po';
import { UserManagementPage } from '../pages/UserManagementPage.po';
import { newUser, editUser } from '../data/constants';
import { routes } from '../data/routes';
import { expect } from '../utils/chai';
import { BrowserActions } from '../utils/browser';

const { firstName, lastName, userName, primaryEmail } = newUser;
const { newUserName } = editUser;
const { baseURL } = routes;

const loadUsersPage: LoadUsersPage = new LoadUsersPage();
const userManagementPage: UserManagementPage = new UserManagementPage();

Given(/^The User is on the "Load Users" page$/, async () => {
    await BrowserActions.navigateTo(baseURL);
});

Given(/^clicks on the "Add User" button$/, async () => {
    await loadUsersPage.openAddUserForm();
});

When(/^he fills in all required fields and submit the form$/, async () => {
    await userManagementPage.addNewUser(firstName, lastName, userName, primaryEmail);
});

Then(/^the page contains a message which says "([^"]*)"$/, async (header: string) => {
    expect(await userManagementPage.getPopupTextHeader()).equal(header);
    await userManagementPage.confirmPopup();
});

Then(/^a new record with provided information is added to the "Users" table$/, async () => {
    await loadUsersPage.searchBy(userName);
    expect(await userManagementPage.getSearchResultsCount()).equal(1);
    expect(await userManagementPage.getSearchResultsValues())
        .to.be.an('array')
        .that.to.include.members([userName]);
});
```

#### Writing Page Objects
```
import { ElementFinder, element, by, ElementArrayFinder } from 'protractor';
import { Actions } from '../utils/actions';
import { CustomWaits } from '../utils/waits';

export class UserManagementPage {
    public firstName: ElementFinder;
    public lastName: ElementFinder;
    public userName: ElementFinder;
    public primaryEmail: ElementFinder;
    public saveBtn: ElementFinder;
    public popUpHeader: ElementFinder;
    public popUpConfirmBtn: ElementFinder;
    public progressbar: ElementFinder;
    public mask: ElementFinder;
    public searchResults: ElementArrayFinder;
    public searchResultsValues: ElementArrayFinder;

    constructor() {
        this.firstName = element(by.name('firstName'));
        this.lastName = element(by.name('lastName'));
        this.userName = element(by.name('userName'));
        this.primaryEmail = element(by.name('primaryEmail'));
        this.saveBtn = element(by.xpath("//span[contains(text(),'Save')]/ancestor-or-self::a"));
        this.popUpHeader = element(
            by.xpath("//*[contains(@id, 'messagebox') and contains(@id, 'header-title-textEl')]"),
        );
        this.popUpConfirmBtn = element(by.xpath("//*[contains(text(), 'OK')]"));
        this.progressbar = element(by.xpath("//*[@role='progressbar']"));
        this.mask = element
            .all(by.xpath("//*[contains(@class, 'x-mask x-border-box') and contains(@role, 'presentation')]"))
            .last();
        this.searchResults = element.all(by.tagName('tr'));
        this.searchResultsValues = element
            .all(by.tagName('tr'))
            .all(by.xpath("//td[contains(@class, 'x-grid-td')]/child::div[@class='x-grid-cell-inner ']"));
    }

    async fillFirstNameField(firstName: string): Promise<void> {
        await Actions.clearFieldInput(this.firstName);
        await Actions.fillInTheField(this.firstName, firstName);
    }

    async fillLastNameField(lastName: string): Promise<void> {
        await Actions.clearFieldInput(this.lastName);
        await Actions.fillInTheField(this.lastName, lastName);
    }

    async fillUserNameField(userName: string): Promise<void> {
        await Actions.clearFieldInput(this.userName);
        await Actions.fillInTheField(this.userName, userName);
    }

    async fillPrimaryEmailField(primaryEmail: string): Promise<void> {
        await Actions.clearFieldInput(this.primaryEmail);
        await Actions.fillInTheField(this.primaryEmail, primaryEmail);
    }

    async submitAddUserForm(): Promise<void> {
        await Actions.click(this.saveBtn);
    }

    async submitEditUserForm(): Promise<void> {
        await Actions.click(this.saveBtn);
    }

    async addNewUser(firstName: string, lastName: string, userName: string, primaryEmail: string): Promise<void> {
        await this.fillFirstNameField(firstName);
        await this.fillLastNameField(lastName);
        await this.fillUserNameField(userName);
        await this.fillPrimaryEmailField(primaryEmail);
        await this.submitAddUserForm();
    }

    async getPopupTextHeader(): Promise<string> {
        return await Actions.getTextFromElement(this.popUpHeader);
    }

    async confirmPopup(): Promise<void> {
        await Actions.click(this.popUpConfirmBtn);
        await CustomWaits.isNotVisible(this.popUpHeader, 3000);
        await CustomWaits.isNotVisible(this.mask, 25000);
        await CustomWaits.isNotVisible(this.progressbar, 15000);
    }

    async getSearchResultsCount(): Promise<number> {
        return await this.searchResults.count();
    }

    async getSearchResultsValues(): Promise<string[]> {
        return await Actions.getAllText(this.searchResultsValues);
    }
}

```
#### Cucumber Hooks
Following method takes screenshot on failure of each scenario
```
After(async function(scenario: HookScenarioResult): Promise<void> {
    if (scenario.result.status === Status.FAILED) {
        await Actions.attachScreenshot(this);
    }
});
```
#### CucumberOpts Tags
Following configuration shows to call specific tags from feature files
```
cucumberOpts: {
    compiler: 'ts:ts-node/register',
    format: ['json:.tmp/results.json', 'node_modules/cucumber-pretty'],
    require: ['../src/steps/*.ts', '../src/utils/*.ts'],
    strict: true,
    tags: argv.tags || '',
},
```
#### HTML Reports
Currently this project has been integrated with [cucumber-html-reporter](https://github.com/gkushang/cucumber-html-reporter), which is generated in the `.tmp/` folder when you run `npm test`.
They can be customized according to user's specific needs.

![cucumberreporterscreen](https://raw.githubusercontent.com/igniteram/protractor-cucumber-typescript/master/images/cucumberReporter.PNG)
