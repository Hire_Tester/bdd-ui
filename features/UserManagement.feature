Feature: User Management

    @smoke
    Scenario: Verify that a new User is created after submitting the "Add User" form
        Given The User is on the "Load Users" page
        And clicks on the "Add User" button
        When he fills in all required fields and submit the form
        Then the page contains a message which says "Created successfully"
        And a new record with provided information is added to the "Users" table

    @smoke
    Scenario: Verify that an existing User is updated after submitting the "Edit User" form
        Given The User is on the "Load Users" page
        And finds the User for editing then clicks on the "Edit" button
        When he fills in the "Username" field and submit the form
        Then a record is updated with provided information in the "Users" table

    @smoke
    Scenario: Verify that an existing User is deleted after clicking the "Delete" button on the "Edit User" form
        Given The User is on the "Load Users" page
        And finds the User for deleting then clicks on the "Edit" button
        When he clicks the "Delete" button and confirms a popup
        Then a record with given user's details is deleted from the "Users" table