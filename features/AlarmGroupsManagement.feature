Feature: Alarm groups Management

    @smoke
    Scenario: Verify that a new alarm group is created after submitting the "New Alarm Group" form
        Given The User is on the "Load Users" page
        And clicks on the "Add Alarm Group" button
        When he fills in "Group Name" field and submit the form
        And clicks on the "Edit Alarm Group" button
        Then the "Alarm Groups" dropdown contains a newly created alarm group

    @smoke
    Scenario: Verify that an existing User can be moved to another alarm group
        Given The User is on the "Load Users" page
        And clicks on the "Edit Alarm Group" button
        When he chooses the User from the "User Groups" user's list and a particular alarm group from dropdown
        And clicks on the "Move" button then submit the form
        Then the User is displayed among users of this group

    @smoke
    Scenario: Verify that an existing User can be removed from a particular alarm group
        Given The User is on the "Load Users" page
        And clicks on the "Edit Alarm Group" button
        When he chooses the User from the "Alarm Groups" user's list and a particular alarm group from dropdown
        And clicks on the "Remove Selected Users" button then submit the form
        Then the User is removed from users of this group